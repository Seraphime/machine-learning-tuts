{-
    This code is released under BSD-V3 licence
        May it serve you as it can

    @author Seraphime Kirkovski
 -}

module Main where

import Prelude hiding ( lines )
import System.Posix.Files ( fileExist, fileAccess )
import System.Directory ( doesDirectoryExist )
import System.IO hiding ( hGetContents )
import System.IO.Unsafe ( unsafePerformIO ) -- I am really tired and really pissed of
import Data.Text ( Text (..), split, lines )
import Data.Text.IO ( hGetContents )
import Data.Text.Read
import qualified Data.Vector as V
import qualified Data.Matrix as M
import System.Environment ( getArgs, getProgName )
import System.Exit ( die, exitFailure )
import GradientDescent  ( gradientDescentMultivariate,
                                 simpleLinearDerivative )
import Graphics.Rendering.Chart.Easy
import Graphics.Rendering.Chart.Backend.Cairo

errorQuit :: String -> IO a
errorQuit = die . (\s -> "\ESC[0;31m-[X]-" ++ s ++ "\ESC[0m")

checkDataSet :: Int -> [a] -> IO ()
{-# INLINE checkDataSet #-}
checkDataSet len arr = if length arr /= len
                        then errorQuit "Inconsistent dataset"
                        else return ()

checkDouble :: (Either a (Double, Text)) -> Double
checkDouble (Left _)  = unsafePerformIO $ errorQuit "Illegal data in input file"
checkDouble (Right (a, _)) = a

toDouble :: [Text] -> [Double]
toDouble = map (\t -> checkDouble $ double t )

parseFile :: Handle -> IO (M.Matrix Double, [Double])
parseFile h
    = do
        conts <- hGetContents h
        let dataset  = map toDouble $ map (split ( == ',')) $ lines conts
        let len      = length $ head dataset
        _     <- mapM_ ( checkDataSet len ) dataset
        let (xs, ys) = unzip $ map (splitAt (len - 1)) dataset
        _     <- hClose h
        return (M.fromLists xs, concat ys)

openInputFile :: FilePath -> IO (Either String (IO Handle))
openInputFile filename
    = do
        exist <- fileExist filename
        perm  <- fileAccess filename True False False
        isdir <- doesDirectoryExist filename
        return $ case (exist, perm, isdir) of
            (False, _, _)     -> Left "File does not exist"
            (_, False, _)     -> Left "Don't have permission to read the file"
            (_, _, True )     -> Left "File is a directory"
            _                 -> Right $ openFile filename ReadMode

dotProduct :: V.Vector Double -> V.Vector Double -> Double
dotProduct a b = V.sum $ V.zipWith (*) a b

scaleMean :: M.Matrix Double -> M.Matrix Double
scaleMean xs
    = M.fromLists $ map doScale (M.toLists xs)
    where
        doScale :: [Double] -> [Double]
        doScale ds
           = let max     = maximum ds
                 min     = minimum ds
                 avg     = sum ds / (fromIntegral $ length ds)
                 in  map (\x -> (x - (max / 2.0)) / (max - min)) ds

getPoints :: V.Vector Double -> Double -> [(Double, Double)]
getPoints t max
    = (0, dotProduct t $ newVect 0.0):(max, dotProduct t $ newVect max) : []
        where
            newVect x = V.singleton  1 `mappend` V.replicate (length t)  x

help :: IO ()
help = getProgName >>= putStr >> putStrLn ": <IN FILE> <alpha :: Double> <OUT FILE>" >> exitFailure

getScaling :: String -> (M.Matrix Double -> M.Matrix Double)
getScaling "ScaleMean" = scaleMean
getScaling _           = id

showTheta :: Double -> Integer -> String
showTheta t n = "\t\ESC[0;36mtheta" ++ (show n) ++ " = " ++ (show  t) ++ "\ESC[0m\n"

main :: IO ()
main = do
    args <- getArgs
    _    <- if length args < 3 then help else return ()
    let alpha  = read (args !! 1 ) :: Double
    let output = args !! 2
    let scaleF = if length args > 3 then getScaling (args !! 3) else id
    eitherhandle <- openInputFile (args !! 0)
    _            <- putStrLn "\ESC[0;33m-[|]-Opening file...\ESC[0m"
    handle <- case eitherhandle of
        Left err        -> errorQuit err
        Right h         -> h
    (xs, ys)     <- parseFile handle

    _            <- putStrLn "\ESC[0;32m-[Y]-File open\ESC[0m"
    _            <- putStrLn "\ESC[0;33m-[|]-Calculating gradient descent...\ESC[0m"

    let eitherth = gradientDescentMultivariate ((scaleF xs), V.fromList ys, alpha) simpleLinearDerivative
    thetas       <- case eitherth of
                    Left err -> errorQuit err
                    Right t  -> return t

    _            <- putStr   "\ESC[0;32m-[Y]-Gradient descent calculated:\n\ESC[0m"
    _            <- putStr $ concat ((zipWith showTheta (V.toList thetas) [0..]))
    _            <- putStrLn "\ESC[0;33m-[|]-Generating plot..\ESC[0m."
    toFile def output $ do
        layout_title .= (args !! 0) ++ " Linear regression"
        plot (points "Expected values" (zip (V.toList (M.getCol 1 xs)) ys))
        plot (line   "Hypothesis"  [getPoints thetas (maximum ys)])
    putStrLn "\ESC[0;32m-[Y]-Plot generated.\nExiting!\ESC[0m"
