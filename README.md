Some code written while following the stanford machine learning course on
coursera: 

https://www.coursera.org/learn/machine-learning/

Everything written is supposed to be played with in ghci.
Currently depends on Data.Matrix.

To try it out, you should first:

cabal install matrix # cabal file is coming 

then you can load in ghci the file you wish to play with.