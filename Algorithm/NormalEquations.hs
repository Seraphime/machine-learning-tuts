module NormalEquations where

import Numeric.LinearAlgebra.HMatrix

-- This is by no means complete. Adding it for the sake of completeness
-- I don't really know the hmatrix package and allf of its features
-- to really do something worth it.

-- | Proxy function to call more easily 'normalEquations'.
-- Transforms input lists to a matrix and vector passes them
-- to 'normalEquations'.
normEquationsLists :: [Double] -> [Double] -> Vector Double
normEquationsLists xs ys = normalEquations xmat yvect where
     xmat    = ((length ys) >< ((length xs)`div`(length ys))) xs
     yvect   = vector ys

-- | Straightforward implementation of normal equations
normalEquations :: Matrix Double -> Vector Double -> Vector Double
normalEquations xs ys
    | rows xs /= size ys       = error "Inconsistent number of training data rows and expected values"
    | otherwise     = (pinv (x' `mul` xs)) `mul` x'  `app` ys
        where
        x'      = tr xs


x = [3.0, 6.0, 9.0]
y = [6.0, 12.0, 18.0]
