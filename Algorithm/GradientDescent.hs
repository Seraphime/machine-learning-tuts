{-
    This code is released under BSD 3 licence.
        May it serve you as it can.

    @author Seraphime Kirkovski
-}

module GradientDescent (gradientDescentMultivariate,
                        gradientDescentMultivariate',
                        simpleLinearDerivative,
                        logisticDerivative,
                        logisticDerivative',
                        innerProductSquare,
                        regGradientDescentMulti,
                        extendInputValues) where

import qualified Data.Vector as V
import qualified Data.Matrix as M

type LearningSet carrier  = (carrier Double,  -- input values
                             V.Vector Double, -- output values
                             Double)          -- learning rate : alpha

type ThetaArgs      a     = (a, a)
type ThetaVector    a     = V.Vector a
type DerivativeSig  a b   = ThetaVector a
                             -> V.Vector a -- expected values
                             -> M.Matrix a -- input values
                             -> Int        -- current column
                             -> b          -- return value

-- | Calculates gradient descent given a matrix of features,
-- a vector of expected values and a derivative of the hypothesis.
gradientDescentMultivariate :: LearningSet M.Matrix
                            -> (DerivativeSig Double Double)
                            -> Either String (ThetaVector Double)
gradientDescentMultivariate (mat, ys, alpha) deriv
    | M.nrows mat /= length ys  = error "len(input values) /= len(output values)"
    | otherwise                 = gradientDescent' (V.replicate (M.ncols m') 0.0) $
                                                   (V.replicate (M.ncols m') 10000.0)
    where
       gradientDescent' thetas oldDiffs =
        let diffs       = map (deriv thetas ys m') [ 0 .. ( M.ncols m' - 1 ) ]
            newThetas   = V.zipWith (\t d -> t - alpha * d / lenYs) thetas diffsV
            diffsV      = V.fromList diffs
            in
            case (safeHead $ filter (/= (Right True)) $ V.toList $ V.zipWith (checkConvergence) diffsV oldDiffs) of
                Nothing         -> Right thetas
                Just (Left err) -> Left err
                _               -> gradientDescent' newThetas (diffsV)
       lenYs    = fromIntegral $ length ys
       m'       = extendInputValues mat

-- | Calculates gradient descent given a matrix of features,
-- a vector of expected values and a list of derivatives - one for each theta.
-- It does not add a zero column to the input values, because the derivative
-- cannot be deduced. Use 'extendInputValues' and add the respective derivative
-- if you want a zero column in your matrix.
gradientDescentMultivariate' :: LearningSet M.Matrix
                               -> [(DerivativeSig Double Double)]
                               -> Either String (ThetaVector Double)
gradientDescentMultivariate' (mat, ys, alpha) ds
    | M.nrows mat /= length ys  = error "len(input values) /= len(output values)"
    | M.ncols mat /= length ds  = error "Number of derivatives /= Number of features"
    | otherwise                 = gradientDescent' $ V.replicate (M.ncols mat) 0.0
    where
       gradientDescent' thetas =
        let diffs       = zipWith (\d c -> d thetas ys mat c) ds [ 0 .. ( M.ncols mat - 1 ) ]
            newThetas   = V.zipWith (\t d -> t - alpha * d / lenYs) thetas (V.fromList diffs)
            in
            case (safeHead $ filter ( /= (Right True) ) $ map (isProbablyZero) diffs) of
                Nothing         -> Right thetas
                Just (Left err) -> Left err
                _               -> gradientDescent' newThetas
       lenYs                      = fromIntegral $ length ys

-- | Calculates regularized gradient descent given a matrix of features,
-- a vector of expected values, lambda and a derivative of the hypothesis.
regGradientDescentMulti     :: Double
                            -> LearningSet M.Matrix
                            -> (DerivativeSig Double Double)
                            -> Either String (ThetaVector Double)
regGradientDescentMulti   lambda (mat, ys, alpha) deriv
    | M.nrows mat /= length ys  = error "len(input values) /= len(output values)"
    | otherwise                 = gradientDescent' (V.replicate (M.ncols m') 0.0) $
                                                   (V.replicate (M.ncols m') 10000.0)
    where
       gradientDescent' thetas oldDiffs =
        let diffs       = map (deriv thetas ys m') [ 0 .. ( M.ncols m' - 1 ) ]
            newThetas   = V.zipWith (\t d -> t * (1 - alpha * lambda / lenYs) - alpha * d / lenYs) thetas diffsV
            diffsV      = V.fromList diffs
            in
            case (safeHead $ filter (/= (Right True)) $ V.toList $ V.zipWith (checkConvergence) diffsV oldDiffs) of
                Nothing         -> Right thetas
                Just (Left err) -> Left err
                _               -> gradientDescent' newThetas (diffsV)
       lenYs    = fromIntegral $ length ys
       m'       = extendInputValues mat


-- | As it name suggests it, it is the derivative of the linear hypothesis function
-- (h(x^i) - y^i) * x^i summed over 1 through i where the ^ is not a power operator
-- but index.
simpleLinearDerivative :: DerivativeSig Double Double
simpleLinearDerivative t ys m' c
    = sum $ map (\r -> ( ( ( innerProduct (M.getRow r m') t ) ) - (ys V.! (r - 1)) )
                                * ( M.getElem r (c + 1) m' ) ) [ 1 .. M.nrows m' ]

-- | For logistic regression
logisticDerivative :: DerivativeSig Double Double
{-# INLINE logisticDerivative #-}
logisticDerivative = logisticDerivative' innerProduct

logisticDerivative' :: ( V.Vector Double
                       -> V.Vector Double
                       -> Double )
                       -> DerivativeSig Double Double
logisticDerivative' f t ys m c
    = sum $ map (\r -> ( ( recip (1.0 + exp ( negate ( f t (M.getRow r m) ) ) ) )
            - ( ys V.! (r - 1) ) ) * ( M.getElem r (c + 1) m ) ) [ 1 .. M.nrows m ]

innerProduct :: (Num a) => V.Vector a -> V.Vector a -> a
{-# INLINE innerProduct #-}
innerProduct v1 v2 = V.sum $ V.zipWith (*) v1 v2

innerProductSquare :: (Num a) => V.Vector a -> V.Vector a -> a
{-# INLINE innerProductSquare #-}
innerProductSquare v1 v2 = V.sum $ V.zipWith (\t x -> t * (x * x)) v1 v2


roundThetaArgs :: ThetaArgs Double -> ThetaArgs Integer
roundThetaArgs (x, y) = (round x, round y)

isProbablyZero x = if isNaN x
                      then Left "One of the derivatives became NaN. Consider lowering alpha"
                      else Right $ x == 0.0 || (abs x) < 0.00001

checkConvergence :: Double -> Double -> Either String Bool
checkConvergence x y
    = if isNaN x
         then Left "One of the derivatives became NaN. Consider lowering alpha"
         else Right (x == 0.0 || (abs x) < 0.000001
                    || ((abs x) < 1 && (abs y) < 1 && abs (x - y) < 0.00001))

extendInputValues :: (Num a) => M.Matrix a -> M.Matrix a
{-# INLINE extendInputValues #-}
extendInputValues mat
  = M.fromLists $
        map (\n -> V.toList $ V.singleton 1 `mappend` M.getRow n mat) [1 .. M.nrows mat]


x = V.fromList [3.0, 6.0, 9.0]
y = V.fromList [6.0, 12.0, 18.0]

x1 = V.fromList [3.0, 6.0, 9.0]
y1 = V.fromList [6.5, 12.5, 18.5]

x2 = M.fromLists [[3.0], [6.0], [9.0]]

-- y = x1 - 2 * x2
x3 = M.fromLists [[1.0, 4.0], [3.0, 1.5], [8.0, 4.0]]
y3 = V.fromList [7.0, 0.0, 0.0]

-- x3 in
-- y = x1^2 + 2 * x0 + 1
y4 = V.fromList [10.0, 13.0, 73.0]

-- logistic regression tests
-- truth table for "and"
xAnd = M.fromLists [[1.0, 1.0], [0.0, 1.0], [1.0,0.0]]
yAnd = V.fromList [1.0, 0.0, 0.0]

safeHead :: [a] -> Maybe a
safeHead []     = Nothing
safeHead (x:_)  = Just x
